package sudoku;

import java.util.HashSet;
import java.util.Set;

public class Sudoku implements SudokuSolver {
	private int[][] grid;

	public Sudoku() {
		grid = new int[9][9];
	}

	/**
	 * Solves the sudoku board if it is solvable.
	 * 
	 * @return boolean solveable Returns false if unsolvable, else true
	 */
	@Override
	public boolean solve() {
		return solver(0, 0);
	}

	/*
	 * Recursive back-tracking method for solving the sudoku grid.
	 */
	private boolean solver(int row, int col) {
		if (row == 9) {
			return true;
		}

		int nextRow = 0;
		int nextCol = 0;
		if (col == 8) {
			nextRow = row + 1;
			nextCol = 0;
		} else {
			nextCol = col + 1;
			nextRow = row;
		}

		if (get(row, col) == 0) { // if the space is empty
			for (int k = 1; k <= 9; k++) {
				add(row, col, k);
				if (isValid()) {
					if (solver(nextRow, nextCol)) {
						return true;
					}
				}
			}
			remove(row, col);
			return false;
		}

		else { // if the space is filled
			if (isValid()) {
				if (solver(nextRow, nextCol)) {
					return true;
				}
			}
			return false;
		}
	}

	/**
	 * Puts digit in the box row, col.
	 * 
	 * @param row   The row
	 * @param col   The column
	 * @param digit The digit to insert in box row, col
	 * @throws IllegalArgumentException if row, col or digit is outside the range
	 *                                  [0..9]
	 */
	@Override
	public void add(int row, int col, int digit) {
		if (row > 9 || col > 9 || digit > 9 || row < 0 || col < 0 || digit < 0) {
			throw new IllegalArgumentException();
		}
		grid[row][col] = digit;

	}

	/**
	 * Removes digit from the box row, col.
	 * 
	 * @param row   The row
	 * @param col   The column
	 * @param digit The digit to remove from box row, col
	 * @throws IllegalArgumentException if row or col is outside the range [0..9]
	 */
	@Override
	public void remove(int row, int col) {
		if (row > 9 || col > 9 || row < 0 || col < 0) {
			throw new IllegalArgumentException();
		}
		grid[row][col] = 0;
	}

	/**
	 * Gets digit in the box row, col.
	 * 
	 * @param row   The row
	 * @param col   The column
	 * @param digit The digit to get from box row, col
	 * @throws IllegalArgumentException if row or col is outside the range [0..9]
	 * @return the digit in box row, col
	 */
	@Override
	public int get(int row, int col) {
		if (row > 8 || col > 8 || row < 0 || col < 0) {
			throw new IllegalArgumentException();
		}
		return grid[row][col];
	}

	/**
	 * Checks that all filled in digits follows the the sudoku rules.
	 */
	@Override
	public boolean isValid() {
		// Check rows
		for (int n = 0; n < 9; n++) {
			if (!checkRow(n)) {
				return false;
			}
		}

		// Check columns
		for (int n = 0; n < 9; n++) {
			if (!checkCol(n)) {
				return false;
			}
		}

		// Check regions
		for (int k = 0; k < 9; k++) {
			if (!checkRegion(k)) {
				return false;
			}
		}
		return true;
	}

	/*
	 * Checks if the row is valid.
	 */
	private boolean checkRow(int row) {
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < 9; i++) {
			if (grid[row][i] != 0 && set.add(grid[row][i]) == false) {
				return false;
			}
		}
		return true;
	}

	/*
	 * Checks if the column is valid.
	 */
	private boolean checkCol(int col) {
		Set<Integer> set = new HashSet<>();
		for (int i = 0; i < 9; i++) {
			if (grid[i][col] != 0 && set.add(grid[i][col]) == false) {
				return false;
			}
		}
		return true;
	}

	/*
	 * Checks if the region is valid.
	 */
	private boolean checkRegion(int region) {
		int startRow = 0;
		if (region == 0 || region == 1 || region == 2) {
			startRow = 0;
		} else if (region == 0 || region == 1 || region == 2) {
			startRow = 3;
		} else {
			startRow = 6;
		}

		int startCol = 0;
		if (region == 0 || region == 3 || region == 7) {
			startCol = 0;
		} else if (region == 1 || region == 4 || region == 8) {
			startCol = 3;
		} else {
			startCol = 6;
		}

		Set<Integer> set = new HashSet<>();
		for (int r = startCol; r < startCol + 3; r++) {
			for (int c = startRow; c < startRow + 3; c++) {
				if (grid[r][c] != 0 && set.add(grid[r][c]) == false) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Empties the entire sudoku board from all digits.
	 */
	@Override
	public void clear() {
		for (int r = 0; r < 9; r++) {
			for (int c = 0; c < 9; c++) {
				grid[r][c] = 0;
			}
		}

	}

	/**
	 * Fills the grid with the digits in m. The digit 0 represents an empty box.
	 * 
	 * @param m the matrix with the digits to insert
	 * @throws IllegalArgumentException if m has the wrong dimension or contains
	 *                                  values outside the range [0..9]
	 */
	@Override
	public void setMatrix(int[][] m) {
		for (int r = 0; r < 9; r++) {
			for (int c = 0; c < 9; c++) {
				if (m[r][c] > 9 || m[r][c] < 0) {
					throw new IllegalArgumentException();
				} else {
					grid[r][c] = m[r][c];
				}
			}
		}
	}

	/**
	 * Gets the grid with currently entered digits.
	 * 
	 * @return the sudoku grid with currently entered digits.
	 */
	@Override
	public int[][] getMatrix() {
		return grid;
	}

}
