package testsudoku;

import static org.junit.jupiter.api.Assertions.*;

import java.util.NoSuchElementException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import sudoku.Sudoku;
import sudoku.SudokuSolver;

class TestSudoku {
	private SudokuSolver sudoku;

	/*
	 * Skriv testfall med JUnit för metoderna i din sudokuklass. Bland testmetoderna
	 * ska ingå lösning av ett tomt sudoku, lösning av sudokut i figur 1 samt försök
	 * att lösa lämpliga fall av olösliga sudokun. Alla publika metoder ska ha
	 * anropats åtminstone en gång i testklassen.
	 * 
	 * Interfacet SudokuSolver ska användas som typ för sudokulösaren i testklassen.
	 * Det är bara när sudokulösarobjektet skapas som du ska använda ditt eget
	 * klassnamn.
	 */

	@BeforeEach
	void setUp() throws Exception {
		sudoku = new Sudoku();
	}

	@AfterEach
	void tearDown() throws Exception {
		sudoku = null;
	}

	/**
	 * Test if an empty sudoku is solvable.
	 */
	@Test
	void testSolveEmpty() {
		assertTrue(sudoku.solve(), "Cannot solve empty sudoku");
		sudoku.clear();
	}

	/**
	 * Test if a solvable sudoku is solvable.
	 */
	@Test
	void testSolveSolvableSudoku() {
		int[][] grid = new int[][] { { 0, 0, 1, 0, 0, 6, 4, 8, 0 }, { 0, 0, 0, 0, 5, 0, 1, 6, 0 },
				{ 8, 0, 2, 0, 0, 0, 0, 0, 0 }, { 0, 0, 5, 2, 0, 0, 6, 0, 0 }, { 0, 0, 0, 1, 0, 0, 0, 3, 0 },
				{ 9, 0, 0, 0, 0, 0, 8, 0, 0 }, { 0, 0, 0, 0, 6, 0, 0, 1, 4 }, { 6, 0, 0, 9, 0, 2, 0, 0, 0 },
				{ 2, 5, 0, 0, 0, 8, 0, 0, 0 } };
		sudoku.setMatrix(grid);
		assertTrue(sudoku.solve(), "Cannot solve solvable sudoku");
		sudoku.clear();
	}
	/*
	 * 1. Fyll i två 5:or på första raden i sudokut. 2. Klicka på Solve-knappen. 3.
	 * Kontrollera att programmet ger besked om att sudokut ej går att lösa. 4.
	 * Upprepa punkt 1-3 med två 5:or i första kolumnen (men ej i samma region). 5.
	 * Upprepa punkt 1-3 med två 5:or i regionen längst upp till vänster (men ej i
	 * samma rad eller kolumn).
	 */

	/**
	 * Test if unsolvable sudokus are solvable.
	 */
	@Test
	void testSolveUnsolveableSudoku() {
		sudoku.add(0, 0, 5);
		sudoku.add(0, 4, 5);
		assertTrue(!sudoku.solve(), "Incorrectly solves unsolvable sudoku");
		sudoku.clear();

		sudoku.add(0, 0, 5);
		sudoku.add(4, 0, 5);
		assertTrue(!sudoku.solve(), "Incorrectly solves unsolvable sudoku");
		sudoku.clear();

		sudoku.add(0, 0, 5);
		sudoku.add(2, 2, 5);
		assertTrue(!sudoku.solve(), "Incorrectly solves unsolvable sudoku");
		sudoku.clear();
	}
	/**
	 * Test if remove method works.
	 */
	@Test
	void testRemoveMethod() {
		sudoku.add(0,0,1);
		sudoku.add(0,1,2);
		sudoku.add(0,2,3);
		sudoku.add(1,0,4);
		sudoku.add(1,1,5);
		sudoku.add(1,2,6);
		sudoku.add(2,3,7);
		assertTrue(!sudoku.solve(), "Incorrectly solves unsolvable sudoku");
		
		sudoku.remove(2,3);
		assertTrue(sudoku.solve(), "Cannot solve solvable sudoku");
		sudoku.clear();
	}
	/**
	 * Test if clear method works.
	 */
	@Test
	void testClearMethod() {
		sudoku.add(0, 0, 5);
		sudoku.add(0, 3, 5);
		assertTrue(!sudoku.solve(), "Incorrectly solves unsolvable sudoku");

		sudoku.clear();
		assertTrue(sudoku.solve(), "Cannot solve solvable sudoku");
		sudoku.clear();
	}
	
	/**
	 * Test enter incorrect input.
	 */
	@Test 
	void testIncorrectInput() {
		assertThrows(IllegalArgumentException.class, () -> sudoku.add(4,4,-1));
		//assertThrows(IllegalArgumentException.class, () -> sudoku.add(4,4,0)); hanteras separat i den grafiska klassen
		assertThrows(IllegalArgumentException.class, () -> sudoku.add(4,4,10));
		assertThrows(IllegalArgumentException.class, () -> sudoku.add(4,4,'a'));
		sudoku.clear();
	}
	
	/**
	 * Test method get().
	 */
	@Test
	void testGetMethod() {
		sudoku.add(0,0,1);
		assertEquals(1, sudoku.get(0,0), "Incorrect get method");
		sudoku.clear();
	}
	/**
	 * Test method isValid().
	 */
	@Test
	void testIsValid() {
		assertTrue(sudoku.isValid());
		sudoku.clear();

		sudoku.add(0,0,1);
		assertTrue(sudoku.isValid());
		sudoku.clear();
	}
	
}
