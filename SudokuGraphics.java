package sudoku;

import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Font;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.Random;

public class SudokuGraphics {

	public static void main(String[] args) {
		SudokuSolver sudoku = new Sudoku();
		int size = 50;

		JFrame frame = new JFrame("Sudoku");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container pane = frame.getContentPane();

		GridLayout gridLayout = new GridLayout(9, 9);
		Font font = new Font("Courier", Font.BOLD, 30);

		// Sudoku grid
		JPanel textPanel = new JPanel();
		JTextField[][] textGrid = new JTextField[9][9];
		textPanel.setLayout(gridLayout);
		for (int r = 0; r < 9; r++) {
			for (int c = 0; c < 9; c++) {
				textGrid[r][c] = new JTextField();
				textGrid[r][c].setBorder(new LineBorder(Color.black));
				textGrid[r][c].setFont(font);
				textGrid[r][c].setHorizontalAlignment(JTextField.CENTER);
				if ((c < 3 && r < 3 || c < 3 && r > 5) || (c > 5 && r < 3 || c > 5 && r > 5)
						|| (c > 2 && c < 6 && r < 6 && r > 2)) {
					textGrid[r][c].setBackground(Color.ORANGE);
				}
				textPanel.add(textGrid[r][c]);
			}
		}
		// Buttons
		JPanel buttonPanel = new JPanel();
		JButton solve = new JButton("Solve");
		JButton clear = new JButton("Clear");
		JButton hint = new JButton("Hint");
		buttonPanel.add(solve);
		buttonPanel.add(clear);
		buttonPanel.add(hint);

		clear.addActionListener(event -> {
			sudoku.clear();
			for (int r = 0; r < 9; r++) {
				for (int c = 0; c < 9; c++) {
					textGrid[r][c].setText("");
				}
			}
		});

		solve.addActionListener(event -> {
			long t0 = System.nanoTime();
			int[][] grid = new int[9][9];
			for (int r = 0; r < 9; r++) {
				for (int c = 0; c < 9; c++) {
					if (textGrid[r][c].getText().equals("")) {
						grid[r][c] = 0;
					} else {
						try {
							grid[r][c] = Integer.parseInt(textGrid[r][c].getText());
							if (grid[r][c] == 0) {
								JOptionPane.showMessageDialog(null, "Incorrect input (0 not allowed)");
								return;
							}
						} catch (NumberFormatException nfe) {
							JOptionPane.showMessageDialog(null, "Incorrect input (not a number)");
							return;
						}
					}
				}
			}
			try {
				sudoku.setMatrix(grid);
			} catch (IllegalArgumentException iae) {
				JOptionPane.showMessageDialog(null, "Incorrect input (incorrect number)");
				return;
			}
			if (!sudoku.solve()) {
				JOptionPane.showMessageDialog(null, "Unsolvable");
				return;
			}
			for (int r = 0; r < 9; r++) {
				for (int c = 0; c < 9; c++) {
					textGrid[r][c].setText("" + sudoku.get(r, c));
				}
			}
			long t1 = System.nanoTime();
			System.out.println("tid: " + (t1 - t0) / 1000000.0 + " ms");
		});

		hint.addActionListener(event -> {
			SudokuSolver temp = new Sudoku();
			Random rand = new Random();
			temp.setMatrix(sudoku.getMatrix());
			temp.solve();
			for (int k = 0; k < 81; k++) {
				int rand1 = rand.nextInt(9);
				int rand2 = rand.nextInt(9);
				if (sudoku.get(rand1, rand2) == 0) {
					sudoku.add(rand1, rand2, temp.get(rand1, rand2));
					textGrid[rand1][rand2].setText("" + sudoku.getMatrix()[rand1][rand2]);
					return;
				}
			}
		});

		// Adding sudoku grid and buttons to frame
		pane.add(textPanel, BorderLayout.CENTER);
		pane.add(buttonPanel, BorderLayout.SOUTH);

		frame.setSize(size * 9, size * 9 + size + 20);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

	}

}
