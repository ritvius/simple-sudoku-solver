package sudoku;

public interface SudokuSolver {

	/**
	 * Solves the sudoku board if it is solvable.
	 * 
	 * @return boolean solveable Returns false if unsolvable, else true
	 */
	boolean solve();

	/**
	 * Puts digit in the box row, col.
	 * 
	 * @param row   The row
	 * @param col   The column
	 * @param digit The digit to insert in box row, col
	 * @throws IllegalArgumentException if row, col or digit is outside the range
	 *                                  [0..9]
	 */
	void add(int row, int col, int digit);

	/**
	 * Removes digit from the box row, col.
	 * 
	 * @param row   The row
	 * @param col   The column
	 * @param digit The digit to remove from box row, col
	 * @throws IllegalArgumentException if row or col is outside the range [0..9]
	 */
	void remove(int row, int col);

	/**
	 * Gets digit in the box row, col.
	 * 
	 * @param row   The row
	 * @param col   The column
	 * @param digit The digit to get from box row, col
	 * @throws IllegalArgumentException if row or col is outside the range [0..9]
	 * @return the digit in box row, col
	 */
	int get(int row, int col);

	/**
	 * Checks that all filled in digits follows the the sudoku rules.
	 */
	boolean isValid();

	/**
	 * Empties the entire sudoku board from all digits.
	 */
	void clear();

	/**
	 * Fills the grid with the digits in m. The digit 0 represents an empty box.
	 * 
	 * @param m the matrix with the digits to insert
	 * @throws IllegalArgumentException if m has the wrong dimension or contains
	 *                                  values outside the range [0..9]
	 */
	void setMatrix(int[][] m);

	/**
	 * Gets the grid with currently entered digits.
	 * 
	 * @return the sudoku grid with currently entered digits.
	 */
	int[][] getMatrix();
}